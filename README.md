# VR_GrabHoldDoor

made for an assessement

## What is this project?

- Door opens inward toward player
- Door opens and aligns with MotionController location 
- Logic forbids door from going over 90 degrees
- Door has a smooth opening and closing rotation
- Ensure hand on door visually works for left & right handed grab 


